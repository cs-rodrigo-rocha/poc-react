# Prova de conceito de React

### O que é esse repositório?
Esse repositório é uma prova de conceito simples com objetivo de decidir qual tecnologia será utilizada para o desenvolvimento do front-end do projeto all-ways

---
### Lugares que eu fui estudando
* https://reactjs.org

___
### Nomes de conceitos usados nessa poc (mais pra caso queiram procurar coisas chave)
* Funcional component
* Class component
* High order component
* Ciclo de vida do componente (embora eu tenha usado só um, ele é largamente usado, hahahha)

___
### Como funciona, resumidamente:
O App, ele é o responsável por renderizar os componentes na index.html. Esses componentes, interagem entre si, compartilhando estados. Um exemplo disso, é o `CardList`, que recebe um conjunto de dados (que a gente chama de props [por vir de outro componente]) de `App` e passa isso para `Card`, que é montado com os dados necessários. O Card, uma vez montado, é como (por de baixo dos panos e em uma explicação bem chula) se retornasse pro Cardlist um html, que é armazenado. Esse conjunto de html retornado do Card pro Cardlist, é retornado para o App, que renderiza isso, no final e exibe pra gente.

O React, funciona através de um negócio chamado `Virtual Dom`, que é a cópia da árvore DOM. Todas essas interações passam primeiro com esse virtual dom, que no futuro, é comparado pelo react com o dom real e muda somente o que é necessário mudar (usando a propriedade de cada componente `key`, como vocês podem ver no Card.

Melhor explanação: https://reactjs.org/docs/state-and-lifecycle.html

Entretanto, não menos importante, o react precisa de uma ferramenta para transpile, pois o navegador *ainda* não aceita. Para isso, eu usei um negócio chamado Webpack, que faz o transpile dele e armazena em um arquivo chamado `bundle.js`, que é consumido no `index.html`

Abraço <3
