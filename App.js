import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

// Componentes
import CardList from './Components/CardList';
import CardComponent from './Components/CardComponent';

// Class-based component, usado quando você armazena estado
class App extends React.Component {
  constructor() {
    super();

    this.state = {
      cardRepos: []
    }

    this.updateCardsList = this.updateCardsList.bind(this);
  }

  updateCardsList(card) {
      this.setState((prevState) => ({
        cardRepos: prevState.cardRepos.concat(card)
      }));
  }



  componentDidMount() {
    this.setState({
      cardRepos: []
    })
  }

  render() {
    return (
      <div>
        <CardComponent cardRepos={this.state.cardRepos} updateCardsList={this.updateCardsList} />

        <CardList cardRepos={this.state.cardRepos} />
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);