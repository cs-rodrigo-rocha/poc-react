import React from 'react';

const Card = (props) => {

  return (
  	<div>
      <h3>{props.card.capitulo}</h3>
      <div>{props.card.categoria}</div>
      <div>{props.card.assunto}</div>
      <div>{props.card.descricao}</div>
    </div>
	);
};

export default Card;
