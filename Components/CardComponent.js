import React from 'react';
import axios from 'axios';

class CardComponent extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit = (event) => {
    event.preventDefault();

    let capitulo = this.capitulo.value;
    let categoria = this.categoria.value;
    let assunto = this.assunto.value;
    let descricao = this.descricao.value;

    let card = {
      capitulo,
      categoria,
      assunto,
      descricao
    }

    this.props.updateCardsList(card)
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <select name="capitulo" ref={(capitulo) => this.capitulo = capitulo}>
          <option value="QA">QA</option>
          <option value="JS">JS</option>
        </select>
        <br/>
        <br/>
        <select name="categoria" ref={(categoria) => this.categoria = categoria}>
          <option value="Categoria 1">Categoria 1</option>
          <option value="Categoria 2">Categoria 2</option>
        </select>
        <br/>
        <br/>
        <input name="assunto" ref={(assunto) => this.assunto = assunto} placeholder='Assunto' type="text" />
        <br/>
        <br/>
        <textarea name="descricao" ref={(descricao) => this.descricao = descricao} rows="4" cols="50"></textarea>
        <br/>
        <br/>
        <button type='submit'>Cadastrar Card</button>
      </form>
    );
  }
}

export default CardComponent;
