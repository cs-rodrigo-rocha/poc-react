import React, { Component } from 'react';

import Card from './Card';

// Functional component => Usado quando você não armazena estado do componente.
const CardList = (props) => {
  return (
    <div>
      {
        props.cardRepos.map((cards, index) => {
          return <Card key={index} card={cards} />;
        } )
      }
    </div>
  );
};

export default CardList;
